<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 17:53
 */

namespace Yearbook\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Yearbook\MainBundle\Entity\Finder;
use Yearbook\MainBundle\Entity\Selector\QuerySelector;
use Yearbook\MainBundle\Form\Type\FinderType;
use Yearbook\MainBundle\Form\Type\UserSelectorType;

class AdminController extends Controller {


    public function findInEntity($entityName,$request){
        $prefix='p';
        $em=$this->getDoctrine()->getManager();
        $tableColumns=$em->getClassMetadata($entityName)->getFieldNames();
        $data=array('tableColumns'=>$tableColumns,'importantColumns'=>array());
        $form=$this->createForm(new UserSelectorType(),new QuerySelector(),array('data'=>$data));
        $form->handleRequest($request);
        if($form->isValid()){
            $dataSelector=$request->request->all();
            $data=$dataSelector['selector'];
            foreach($data as $k => $v){
                if($k==='max'){
                    if(!isset($range)){
                        $range=$v;
                    }
                }
                if($k==='columns'){
                    foreach($v as $val){
                        $columns[]=$prefix.'.'.$val;
                    }
                }
                if($k==='ascDesc'){
                    if(!isset($sort)){
                        $sort=$v;
                    }
                }
            }
            $users=$em->getRepository('YearbookMainBundle:User')->selectTargetedColumns($columns,$range,$sort,$prefix,$entityName);
            return $users;
        }

    }
    public function indexAction(){
        return $this->render('YearbookMainBundle:Admin:index.html.twig');
    }

    public function guideAction(){
        return $this->render('YearbookMainBundle:Admin:guide.html.twig');
    }



    public function userAction(){

        $em=$this->getDoctrine()->getManager();
        $tableColumns=$em->getClassMetadata('YearbookMainBundle:User')->getFieldNames();
        $importantColumns=array('id','username','email','lasLogin','enabled');
        $data=array('tableColumns'=>$tableColumns,'importantColumns'=>$importantColumns);
        $form=$this->createForm(new UserSelectorType(),new QuerySelector(),array('data'=>$data,));
        //$users=$em->getRepository('YearbookMainBundle:User')->selectNumberRows();
        return $this->render('YearbookMainBundle:Admin:user.html.twig',array('form'=>$form->createView()));
    }

    public function userFindAction(Request $request){
        $users=$this->findInEntity('YearbookMainBundle:User',$request);
        return $this->render('YearbookMainBundle:Admin:user.html.twig',array('content'=>$users));
    }

    public function findAction(){
        $finder=new Finder();
        $form=$this->createForm(new FinderType(),$finder,array('data'=>array()));
        return $this->render('YearbookMainBundle:Admin:find.html.twig',array('form'=>$form->createView()));
    }

    public function findPostAction(Request $request){
        $em=$this->getDoctrine()->getManager();
        $data=$request->request->all();
        $class=array();
        foreach($data as $v){
            foreach($v as $key => $value){
                $class[$key]=$value;
            }
        }
        $tableColumns=$em->getClassMetadata('YearbookMainBundle:'.$data['finder']['class'])->getFieldNames();
        $finder=new Finder();
        $form=$this->createForm(new FinderType(),$finder,array('data'=>array('class'=>$class,'tableColumns'=>$tableColumns)));
        return $this->render('YearbookMainBundle:Admin:find.html.twig',array('form'=>$form->createView()));
    }

    public function organizationAction(){
        $em=$this->getDoctrine()->getManager();
        $tableColumns=$em->getClassMetadata('YearbookMainBundle:Organization')->getFieldNames();
        $importantColumns=array('city');
        $data=array('tableColumns'=>$tableColumns,'importantColumns'=>$importantColumns);
        $form=$this->createForm(new UserSelectorType(),new QuerySelector(),array('data'=>$data,));
        return $this->render('YearbookMainBundle:Admin:organization.html.twig',array('form'=>$form->createView()));
    }

    public function yearbookAction(){

        $em=$this->getDoctrine()->getManager();
        $tableColumns=$em->getClassMetadata('YearbookMainBundle:Yearbook')->getFieldNames();
        $importantColumns=array('id');
        $data=array('tableColumns'=>$tableColumns,'importantColumns'=>$importantColumns);
        $form=$this->createForm(new UserSelectorType(),new QuerySelector(),array('data'=>$data,));
        //$users=$em->getRepository('YearbookMainBundle:Yearbook')->selectNumberRows();
        return $this->render('YearbookMainBundle:Admin:yearbook.html.twig',array('form'=>$form->createView()));
    }

    public function yearbookFindAction(Request $request){
        $yearbooks=$this->findInEntity('YearbookMainBundle:Yearbook',$request);
        return $this->render('YearbookMainBundle:Admin:yearbook.html.twig',array('content'=>$yearbooks));
    }

    public function organizationFindAction(Request $request){
        $organizations=$this->findInEntity('YearbookMainBundle:Organization',$request);
        return $this->render('YearbookMainBundle:Admin:organization.html.twig',array('content'=>$organizations));
    }



} 