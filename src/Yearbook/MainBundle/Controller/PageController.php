<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 16.8.14
 * Time: 20:46
 */

namespace Yearbook\MainBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Yearbook\MainBundle\Entity\Email;
use Yearbook\MainBundle\Form\Type\EmailType;
use Yearbook\MainBundle\Form\Type\SecondEmailType;

class PageController extends Controller {
    public function indexAction()
    {
        return $this->render('YearbookMainBundle::layoutGuidePost.html.twig');
    }



    public function userIndexAction(){
        return $this->render('YearbookMainBundle:Page:userInfo.html.twig');
    }


    public function contactAction(){
        $enquiry=New Email();
        $form=$this->createForm(New EmailType(),$enquiry);
    return $this->render('YearbookMainBundle:Page:contactEmail.html.twig',array('form' => $form->createView()));
    }

    public function contactPostAction(Request $request){
        $enquiry=New Email();
        $form=$this->createForm(New EmailType(),$enquiry);
        $form->handleRequest($request);
        if($form->isValid()){
            $user = $this->get('security.context')->getToken()->getUser();
            $userName=$user->getId();
            $userEmail=$user->getEmail();
            $message = \Swift_Message::newInstance()
                ->setSubject('Contact enquiry from symblog')
                ->setFrom('enquiries@honzik.cz')
                ->setTo($this->container->getParameter('yearbook.emails.contact_email'))
                ->setBody($this->renderView('YearbookMainBundle:Page:contactEmail.txt.twig', array('enquiry' => $enquiry,'name'=>$userName,'email'=>$userEmail)));
            $this->get('mailer')->send($message);
            $mes='Váš email byl úspešně odeslán. Na adresu '.$userEmail.' bude zaslána odpověď.';
            $this->get('session')->getFlashBag()->add('emailuserIndex-notice',$mes);
            return $this->redirect($this->generateUrl('yearbook_user_index'));
        }
        return $this->redirect($this->generateUrl('yearbook_user_index'));
    }

    public function emailPostAction(Request $request){
        $user=$this->get('security.context')->getToken()->getUser();
        $userId=$user->getId();
        $form=$this->createForm(new SecondEmailType(),$user);
        $form->handleRequest($request);
        if($request->request->get('unsetEmail')==true){
            $em=$this->getDoctrine()->getManager();
            $us=$em->getRepository('YearbookMainBundle:User')->find($userId);
            $us->setEmail(false);
            $us->setEmailCanonical(false);
            $em->flush();
            $mes='Váš email byl odpojen od Vašeho účtu';
            $this->get('session')->getFlashBag()->add('email-notice',$mes);
            return $this->redirect($this->generateUrl('yearbook_user_change_email'));
        }
        if($form->isValid()){
            $update=$form->getData();
            $em=$this->getDoctrine()->getManager();
            $em->persist($update);
            $em->flush();
            $mes='Váš email byl úspešně připojen k Vašemu účtu';
            $this->get('session')->getFlashBag()->add('email-notice',$mes);
        }
        return $this->redirect($this->generateUrl('yearbook_user_index'));
    }
    public function emailAction(){
        $user=$this->get('security.context')->getToken()->getUser();
        $userEmail=$user->getEmail();
        $form=$this->createForm(new SecondEmailType(),$user);
        if(!$userEmail){
            $secondMailArr['state']=FALSE;
            $secondMailArr['message']='K vašemu účtu není připojen kontakní email.';
        }else{
            $secondMailArr['state']=TRUE;
            $secondMailArr['email']=$userEmail;
            return $this->render('YearbookMainBundle:Page:secondEmail.html.twig',array('secondEmail'=>$secondMailArr,'form'=>$form->createView()));

        }
        return $this->render('YearbookMainBundle:Page:secondEmail.html.twig',array('secondEmail'=>$secondMailArr,'form'=>$form->createView()));
    }
}