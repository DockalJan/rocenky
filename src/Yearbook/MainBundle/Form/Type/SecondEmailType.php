<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 15:08
 */

namespace Yearbook\MainBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SecondEmailType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('email','repeated',array(
            'type' => 'email',
            'first_options'=>array('label'=>'Váš email: '),
            'second_options'=>array('label'=>'Potvrzení emailu: '),
        ));
    }

    public function getName(){
        return 'changeMail';
    }
} 