<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 20.8.14
 * Time: 13:34
 */

namespace Yearbook\MainBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FinderType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options){
        if(isset($options['data']['class'])){
            $categorySelected=$options['data']['class']['class'];
            $columnSelected=$options['data']['class']['var'];
            $operatorSelected=$options['data']['class']['operator'];
            $columns=array_combine($options['data']['tableColumns'],$options['data']['tableColumns']);
        }else{
            $categorySelected='';
            $columns=array();
            $columnSelected='';
            $operatorSelected='';
        }

        $builder->add('class','choice',array(
            'choices'=>array(
                'User'=>'Uživatel',
                'Yearbook'=>'Ročenka',
                'Organization'=>'Organizace',
            ),
            'required'=>true,
            'empty_value'=>'Vyber kategorii',
            'empty_data'=>null,
            'label'=>'Kategorie: ',
            'data'=>$categorySelected
        ));
        $builder->add('var','choice',array(
            'choices'=>array(
                $columns
            ),
            'required'=>false,
            'empty_value'=>'Vyberte sloupec',
            'empty_data'=>null,
            'label'=>'Sloupec: ',
            'data'=>$columnSelected
        ));
        $builder->add('operator','choice',array(
            'choices'=>array(
                '='=>'=',
                '>'=>'>',
                '<'=>'<',
                '!='=>'!=',
            ),
            'required'=>false,
            'empty_value'=>'vyberte prosím',
            'empty_data'=>null,
            'label'=>'Operátor: ',
            'data'=>$operatorSelected
        ));

    }

    public function getName(){
        return 'finder';
    }

} 