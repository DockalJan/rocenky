<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 12:24
 */

namespace Yearbook\MainBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EmailType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('subject','text',array('label'=>'Předmět: '));
        $builder->add('body', 'textarea',array('label'=>'Zpráva: '));
    }

    public function getName()
    {
        return 'contact';
    }
} 