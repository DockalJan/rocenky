<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 20.8.14
 * Time: 18:01
 */

namespace Yearbook\MainBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserSelectorType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(!empty ($options['data'])){
            $col=array_combine($options['data']['tableColumns'],$options['data']['tableColumns']);
            $importantCol=$options['data']['importantColumns'];
        }else{
            $col='';
            $importantCol='';
        }

        $builder->add('max','number',array(
            'label'=>'Počet výsledků na stránku: ',
            'data'=>'10',

        ));

        $builder->add('columns','choice',array(
            'choices'=>array(
                $col
            ),
            'label'=>'Zobrazit sloupce: ',
            'data'=>$importantCol,
            'multiple'=>true,
            'expanded'=>true,
        ));
        $builder->add('ascDesc','choice',array(
            'choices'=>array(
                'asc'=>'Vzestupně',
                'desc'=>'Sestupně',
            ),
            'label'=>'Seřadit: '
        ));

    }
    public function getName(){
        return 'selector';
    }
} 