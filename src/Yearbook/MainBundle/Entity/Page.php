<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 11:38
 */

namespace Yearbook\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 */
class Page {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string",length=150)
     */
    protected $json;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdDate;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $edited;

    /**
     * @ORM\Column(type="integer")
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="Yearbook", inversedBy="page")
     * @ORM\JoinColumn(name="yearbook_id", referencedColumnName="id")
     */
    protected $yearbook;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set json
     *
     * @param string $json
     * @return Page
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string 
     */
    public function getJson()
    {
        return $this->json;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @return Page
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set edited
     *
     * @param boolean $edited
     * @return Page
     */
    public function setEdited($edited)
    {
        $this->edited = $edited;

        return $this;
    }

    /**
     * Get edited
     *
     * @return boolean 
     */
    public function getEdited()
    {
        return $this->edited;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Page
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set yearbook
     *
     * @param \Yearbook\MainBundle\Entity\Yearbook $yearbook
     * @return Page
     */
    public function setYearbook(\Yearbook\MainBundle\Entity\Yearbook $yearbook = null)
    {
        $this->yearbook = $yearbook;

        return $this;
    }

    /**
     * Get yearbook
     *
     * @return \Yearbook\MainBundle\Entity\Yearbook 
     */
    public function getYearbook()
    {
        return $this->yearbook;
    }
}
