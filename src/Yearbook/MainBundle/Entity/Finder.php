<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 20.8.14
 * Time: 13:42
 */

namespace Yearbook\MainBundle\Entity;


class Finder {
    protected $class;
    protected $var;
    protected $operator;
    protected $data;

    public  function getClass(){
        return $this->class;
    }
    public function setClass($class){
        $this->class=$class;
    }
    public function getVar(){
        return $this->var;
    }
    public function setVar($var){
        $this->var=$var;
    }
    public function getOperator(){
        return $this->operator;
    }
    public function setOperator($operator){
        $this->operator=$operator;
    }
    public function getData(){
        return $this->data;
    }
    public function setData($data){
        $this->data = $data;
    }
} 