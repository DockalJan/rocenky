<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 20.8.14
 * Time: 17:50
 */

namespace Yearbook\MainBundle\Entity\Selector;


class QuerySelector {
    protected $max;
    protected $columns;
    protected $ascDesc;

    public function __construct($columns=array()){
        $this->columns=$columns;
    }
    public function getAscDesc(){
        return $this->ascDesc;
    }

    public function setAscDesc($ad){
        $this->ascDesc=$ad;
    }
    public function getMax(){
        return $this->max;
    }

    public function setMax($max){
        $this->max=$max;
    }

    public function addColumns($column){
        if(!$this->hasColumns($column)){
            $this->columns[]=$column;
        }
    }

    public function hasColumns($column){
        return in_array($column,$this->columns,true);
    }

    public function setColumns(array $column){
        $this->columns = $column;
    }



    public function removeColumns($column){
        if( false!== $key = array_search($column,$this->columns, true)){
            unset($this->columns[$key]);
            $this->columns= array_values($this->columns);
        }
    }
    public function getColumns(){
        return $this->columns;
    }
} 