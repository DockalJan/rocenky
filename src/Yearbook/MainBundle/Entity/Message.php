<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 11:12
 */

namespace Yearbook\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="message")
 */
class Message {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $sendTime;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $readTime;

    /**
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_from", referencedColumnName="id")
     */
    protected $sendFrom;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="messages")
     * @ORM\JoinColumn(name="user_to", referencedColumnName="id")
     */
    protected $sendTo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $read;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sendTime
     *
     * @param \DateTime $sendTime
     * @return Message
     */
    public function setSendTime($sendTime)
    {
        $this->sendTime = $sendTime;

        return $this;
    }

    /**
     * Get sendTime
     *
     * @return \DateTime 
     */
    public function getSendTime()
    {
        return $this->sendTime;
    }

    /**
     * Set readTime
     *
     * @param \DateTime $readTime
     * @return Message
     */
    public function setReadTime($readTime)
    {
        $this->readTime = $readTime;

        return $this;
    }

    /**
     * Get readTime
     *
     * @return \DateTime 
     */
    public function getReadTime()
    {
        return $this->readTime;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Message
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sendFrom
     *
     * @param \Yearbook\MainBundle\Entity\User $sendFrom
     * @return Message
     */
    public function setSendFrom(\Yearbook\MainBundle\Entity\User $sendFrom = null)
    {
        $this->sendFrom = $sendFrom;

        return $this;
    }

    /**
     * Get sendFrom
     *
     * @return \Yearbook\MainBundle\Entity\User 
     */
    public function getSendFrom()
    {
        return $this->sendFrom;
    }

    /**
     * Set sendTo
     *
     * @param \Yearbook\MainBundle\Entity\User $sendTo
     * @return Message
     */
    public function setSendTo(\Yearbook\MainBundle\Entity\User $sendTo = null)
    {
        $this->sendTo = $sendTo;

        return $this;
    }

    /**
     * Get sendTo
     *
     * @return \Yearbook\MainBundle\Entity\User 
     */
    public function getSendTo()
    {
        return $this->sendTo;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page
     *
     * @param \Yearbook\MainBundle\Entity\Page $page
     * @return Message
     */
    public function addPage(\Yearbook\MainBundle\Entity\Page $page)
    {
        $this->page[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \Yearbook\MainBundle\Entity\Page $page
     */
    public function removePage(\Yearbook\MainBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Get page
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set read
     *
     * @param boolean $read
     * @return Message
     */
    public function setRead($read)
    {
        $this->read = $read;

        return $this;
    }

    /**
     * Get read
     *
     * @return boolean 
     */
    public function getRead()
    {
        return $this->read;
    }
}
