<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 16.8.14
 * Time: 16:58
 */

namespace Yearbook\MainBundle\Entity;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="Yearbook\MainBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Organization", inversedBy="users")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;



    public function __construct(){
        parent::__construct();
        $this->roles=array('ROLE_USER');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organization
     *
     * @param \Yearbook\MainBundle\Entity\Organization $organization
     * @return User
     */
    public function setOrganization(\Yearbook\MainBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return \Yearbook\MainBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set secondEmail
     *
     * @param string $secondEmail
     * @return User
     */
    public function setSecondEmail($secondEmail)
    {
        $this->secondEmail = $secondEmail;

        return $this;
    }

    /**
     * Get secondEmail
     *
     * @return string 
     */
    public function getSecondEmail()
    {
        return $this->secondEmail;
    }
}
