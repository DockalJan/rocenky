<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 19.8.14
 * Time: 12:20
 */

namespace Yearbook\MainBundle\Entity;


class Email {
    protected $name;

    protected $email;

    protected $subject;

    protected $body;

    protected $secondEmail;

    public function getSecondEmail(){
        return $this->secondEmail;
    }

    public function setSecondEmail($secondEmail){
        $this->secondEmail=$secondEmail;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

}