<?php
/**
 * Created by PhpStorm.
 * User: honzik
 * Date: 20.8.14
 * Time: 17:27
 */

namespace Yearbook\MainBundle\Entity\Repository;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository {
    public function selectNumberRows($number=20){
        return $this->getEntityManager()->createQuery(
            'SELECT p FROM YearbookMainBundle:User p where 1=1'
        )
        ->setMaxResults($number)
         ->getResult();
    }

    public function selectTargetedColumns(array $columns,$range=20,$sort='asc',$pref,$entityName){
        $query= $this->getEntityManager()->createQueryBuilder();
        $query
            ->select($columns)
            ->from($entityName,$pref)
        ;
        $query->setMaxResults($range);
        $results=$query->getQuery()->getResult();
        return $results;
    }
} 