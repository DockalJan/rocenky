<?php

namespace Yearbook\MainBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class YearbookMainBundle extends Bundle
{
    public function getParent(){
        return 'FOSUserBundle';
    }
}
